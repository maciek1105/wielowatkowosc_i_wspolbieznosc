﻿using MKasprzyk.WielowątkowośćiWspółbieżność.Model;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MKasprzyk.WielowątkowośćiWspółbieżność
{
    class Program
    {
        static void Main()
        {
            var bookStore = new BookStore();
            var client1 = new Person("Adam", "Małysz", 100.0);
            var client2 = new Person("Kamil", "Stoch", 100.0);
            client1.SetBookStoreAccountBook(bookStore.AccountingBook);
            client2.SetBookStoreAccountBook(bookStore.AccountingBook);

            var books = new Collection<Book>
            { 
                new Book("Mistrz i Małgorzata", "Michaił Bułhakow", 10.0),
                new Book("Biesy", "Fiodor Dostojewski", 8.0), 
                new Book("Lśnienie", "King", 10.0), 
                new Book("Władca pierścieni", "Tolkien", 8.0)
            };

            for(int i =0; i<4; i++)
            {
                bookStore.FillStore(books);
            }

            var chart1 = new Chart();
            //chart1.AddToChart(new Book("Mistrz i Małgorzata", "Michaił Bułhakow", 10.0) , bookStore.Store);
            //chart1.AddToChart(new Book("Biesy", "Fiodor Dostojewski", 8.0), bookStore.Store);

            var chart2 = new Chart();
            //chart2.AddToChart(new Book("Lśnienie", "King", 10.0), bookStore.Store);
            //chart2.AddToChart(new Book("Władca pierścieni", "Tolkien", 8.0), bookStore.Store);            

            var t1 = Task.Run(() => client1.BuyChart(chart1));
            var t2 = Task.Run(() => client2.BuyChart(chart2));

            var t3 = Task.Factory.ContinueWhenAll(new[] { t1, t2 }, (x) =>  bookStore.StoreToString());

            t3.ContinueWith((x) => bookStore.ShowAccountBookLogs()).Wait();
            

            int iiii = 0;

        }
    }
}
