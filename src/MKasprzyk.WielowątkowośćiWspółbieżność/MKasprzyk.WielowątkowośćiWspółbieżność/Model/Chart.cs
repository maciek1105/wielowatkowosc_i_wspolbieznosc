﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class Chart
    {      
        public Chart()
        {
            Value = 0.0;
        }

        public double Value { get; private set; }

        private List<Book> _books;

        public List<Book> Books => _books ?? (_books = new List<Book>());

        public void AddToChart(Book book, IEnumerable<Storage> storage)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (storage is null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            var storageItem = storage.FirstOrDefault(x => x.Book.Title == book.Title && x.Book.Author == book.Author && x.BooksNumber > 0);

            if (storageItem != null)
            {
                Books.Add(book);
                Value += book.Price;               

                storageItem.BooksNumber--;
            }
        }


        public void RemoveFromChart(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            Books.Remove(book);
            Value -= book.Price;
        }
    }
}
