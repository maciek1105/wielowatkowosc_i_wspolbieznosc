﻿using System;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class Book
    {
        public Book(string title, string author, double price)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Author = author ?? throw new ArgumentNullException(nameof(author));
            Price = price;
        }

        public string Title { get; private set; }
        public string Author { get; private set; }
        public double Price { get; set; }

        public override string ToString() => $"{Title} {Author}";     

    }
}
