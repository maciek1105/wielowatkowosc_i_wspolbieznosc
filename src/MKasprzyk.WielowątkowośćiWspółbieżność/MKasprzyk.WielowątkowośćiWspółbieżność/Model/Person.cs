﻿using System;
using System.Collections.ObjectModel;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class Person
    {
        public Person(string name, string surname, double account)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Surname = surname ?? throw new ArgumentNullException(nameof(surname));
            Account = account;
            _books = new Lazy<Collection<Book>>();
        }

        public string Name { get; private set; }
        public string Surname { get; private set; }
        public double Account { get; private set; }

        private Lazy<Collection<Book>> _books;
        private Collection<AccountLog> _bookStoreAccountBook;     

        public void SetBookStoreAccountBook(Collection<AccountLog> accountLogs)
        {
            _bookStoreAccountBook = accountLogs ?? throw new ArgumentNullException(nameof(accountLogs));
        }      

        public void BuyChart(Chart chart)
        {
            if (chart is null)
            {
                throw new ArgumentNullException(nameof(chart));
            }

            foreach (var book in chart.Books)
            {
                _books.Value.Add(book);
                
                var log = new AccountLog(book.Title,$"{Name} {Surname}", book.Price);

                lock (_bookStoreAccountBook)
                {
                    _bookStoreAccountBook.Add(log);
                }
            }

            Account -= chart.Value;                      
        }
    }
}
