﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class Storage
    {
        public Book Book { get; private set; }
        public int BooksNumber { get; set; }

        public Storage(Book books)
        {
            Book = books ?? throw new ArgumentNullException(nameof(books));
            BooksNumber = 0;
        }
    }
}
