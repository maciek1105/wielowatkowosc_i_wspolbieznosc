﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class BookStore
    {
        public Collection<AccountLog> AccountingBook { get; private set; }
        public List<Storage> Store { get; private set; }

        public BookStore()
        {
            AccountingBook = new Collection<AccountLog>();
            Store = new List<Storage>();
        }

        public void FillStore(IEnumerable<Book> books)
        {
            foreach(var book in books.ToList())
            {
                var storageItem = Store.FirstOrDefault(x => x.Book == book);

                if (storageItem != null)
                {
                    storageItem.BooksNumber++;
                }
                else
                {
                    Store.Add(new Storage(book));
                }
            }
        }       

        public void ShowAccountBookLogs()
        {   
            foreach(var log in AccountingBook)
            {
                System.Console.WriteLine(log.ToString());
            }
        }

        public void StoreToString()
        {
            var ret = "";

            foreach(var item in Store.Where(x => x.BooksNumber > 0))
            {
                ret += $"{ item.Book.ToString()} {item.BooksNumber} \n";
            }

            System.Console.WriteLine(ret);
        }
    }
}
