﻿using System;

namespace MKasprzyk.WielowątkowośćiWspółbieżność.Model
{
    public class AccountLog
    {
        private readonly string _title;
        private readonly string _client;
        private readonly double _price;

        public AccountLog(string title, string client, double price)
        {
            _title = title ?? throw new ArgumentNullException(nameof(title));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _price = price;
        }
        public override string ToString() => $"{_client} bought {_title} for {_price}";        
    }
}